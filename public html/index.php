<!DOCTYPE html>
<html>

<head>
    
    <style type="text/css">
        body{
    background-color:#f9f9f9;
    font-size:16px
    color:#444;
    font-family: sans-serif;
}

.content{
    width: 80%;
    margin: 10px auto;
}

/*header*/
header{
    background-color: white;
    padding: 20px 10px;
    border-radius: 5px;
    border: 1px solid #f0f0f0;
    margin-bottom: 10px;
}

header h1.judul,
header h3.deskripsi{
    text-align: center; 
}

/*menu navigasi*/
.menu{
    background-color: #87CEFA;
    border: 1px solid #f0f0f0;
    border-radius: 8px; 
    margin-bottom: 10px;
}

div.menu ul {
    list-style:none;
    overflow: hidden;
}


div.menu ul li {
    float:left;     
    text-transform:uppercase;
}

div.menu ul li a {
    display:block;  
    padding:0 20px;
    text-decoration:none;
    color:#2c2c2c;
    font-family: sans-serif;
    font-size:13px;
    font-weight:400;
    transition:all 0.3s ease-in-out;
}

div.menu ul li a:hover,
div.menu ul li a.hoverover {    
    cursor: pointer;    
    color:#fff;
}


div.badan{
    background-color: white;
    border-radius: 5px;
    border: 1px solid #f0f0f0;
    margin-bottom: 10px;
}

div.halaman{
    text-align: center;
    padding: 30px 20px; 
}
    </style>
</head>
<body>

<div class="content">
    <header>
        <h1 class="judul">blablabla</h1>
        <h3 class="deskripsi">Ini blog fatin</h3>
    </header>
 
    <div class="menu">
        <ul>
            <li><a href="index.php?page=home">HOME</a></li>
            <li><a href="index.php?page=tentang">TENTANG</a></li>
            <li><a href="index.php?page=tutorial">TUTORIAL</a></li>
        </ul>
    </div>
 
    <div class="badan">
 
 
    <?php 
    if(isset($_GET['page'])){
        $page = $_GET['page'];
 
        switch ($page) {
            case 'home':
                include "halaman/home.php";
                break;
            case 'tentang':
                include "halaman/tentang.php";
                break;
            case 'tutorial':
                include "halaman/tutorial.php";
                break;          
            default:
                echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
                break;
        }
    }else{
    }
 
     ?>
 
    </div>
</div>
</body>
</html>